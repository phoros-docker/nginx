FROM alpine:edge

ENV S6_OVERLAY_VERSION v1.21.7.0

# Terminate all s6 childs when init stage fails
ENV S6_BEHAVIOUR_IF_STAGE2_FAILS 2

# Install needed packages
RUN apk add --update --no-cache --no-progress --virtual .build-deps ca-certificates wget \
    && apk add --update --no-cache --no-progress apache2-utils bash inotify-tools nginx openssl \
    && wget https://github.com/just-containers/s6-overlay/releases/download/${S6_OVERLAY_VERSION}/s6-overlay-amd64.tar.gz \
    && apk del --purge .build-deps \
    && tar -C / -xzvf s6-overlay-amd64.tar.gz \
    && rm s6-overlay-amd64.tar.gz \
    && rm -rf /var/cache/apk/* \
    && mkdir -p /run/nginx/ \
    && chown nginx:root /run/nginx/ \
    && chmod 750 /run/nginx/ \
    && ln -sf /dev/stdout /var/log/nginx/access.log && ln -sf /dev/stderr /var/log/nginx/error.log

# Copy the configuration
COPY ./config/ /

# Copy required assets for s6-overlay
COPY ./assets/s6-overlay/ /etc/

# Fix permissions
RUN chmod -R a=r,u+w,a+X /etc/services.d/ /etc/cont-init.d/ /etc/nginx/ \
    && chmod +x /etc/cont-init.d/0* -R \
    && chmod -R 755 /etc/periodic/

ENTRYPOINT ["/init"]
